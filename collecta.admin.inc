<?php

/**
 * @file
 */

/**
 * Menu callback for administration settings.
 */
function _collecta_admin_settings() {
  $form['collecta_widget_title'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Widget title'),
    '#default_value'  => variable_get('collecta_widget_title', NULL),
    '#description'    => t('Provide a title for your collecta widget or leave blank for no title.'),
  );

  $form['collecta_search_terms'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Search term(s)'),
    '#default_value'  => variable_get('collecta_search_terms', NULL),
    '#description'    => t('Enter search terms for widget.'),
  );

  $form['collecta_width'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Width (px)'),
    '#default_value'  => variable_get('collecta_width', NULL),
  );

  $form['collecta_height'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Height (px)'),
    '#default_value'  => variable_get('collecta_height', NULL),
  );
  
  $form['collecta'] = array(
    '#type'         => 'fieldset',
    '#title'        => t('Advanced options'),
    '#collapsible'  => TRUE,
    '#collapsed'    => TRUE,
  );

  $form['collecta']['collecta_external_css'] = array(
    '#type'         => 'textfield',
    '#title'        => t('External stylesheet URL'),
    '#default_value'  => variable_get('collecta_external_css', NULL),
  );

  $form['collecta']['collecta_heading_bg'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Header background URL'),
    '#default_value'  => variable_get('collecta_heading_bg', NULL),
  );

  $form['collecta']['collecta_seconds_per_result'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Seconds per result'),
    '#default_value'  => variable_get('collecta_seconds_per_result', NULL),
  );

  return system_settings_form($form);
}
